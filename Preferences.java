import java.util.HashMap;

public class Preferences
{

	private static volatile Preferences instance;
	private HashMap<String, Object> props = new HashMap<String, Object>();

	private Preferences(){}

	public static Preferences getInstance()
	{
		if (instance == null)
		{
			synchronized (Preferences.class)
			{
				if (instance == null)
				{
					instance = new Preferences();
				}
			}
		}
		return instance;
	}


	public void setProperty(String key, Object val)
	{
		props.put(key, val);
	}


	public Object getProperty(String key)
	{
		if (props.containsKey(key))
		{
			return props.get(key);
		}
		return null;
	}
}