import java.io.*;

class Singleton
{
	public static void main(String args[])
	{
		Preferences prefs = Preferences.getInstance();
		prefs.setProperty("first", "Hello world");
		System.out.println(prefs.getProperty("first"));
	}
}